import 'package:flutter/material.dart';
import 'package:prototipo_v1/screens/home_screen_copy.dart';
import 'package:prototipo_v1/screens/login_screen.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'material app',
      initialRoute: 'login',
      routes: {
        'login': (_) => const LoginScreen(),
        'home': (_) => const HomeScreenCopy(),
      },
    );
  }
}
