import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tflite/tflite.dart';

class HomeScreenCopy extends StatefulWidget {
  const HomeScreenCopy({super.key});

  @override
  State<HomeScreenCopy> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreenCopy> {
  int counter = 0;

  File? imagen;
  bool _busy = false;
  String ruta = "";
  late List _recognitions = [];

  Future analyzeTFLite() async {
    String? res = await Tflite.loadModel(
        model: "assets/model.tflite",
        labels: "assets/labels.txt",
        numThreads: 2 // defaults to 1
        );
    print('Model Loaded: $res');
    var recognitions = await Tflite.runModelOnImage(path: imagen!.path);
    setState(() {
      _recognitions = recognitions!;
    });
    print('Recognition Result: $_recognitions');
  }

  Future chooseImageGallery(int op) async {
    final ImagePicker _picker = ImagePicker();
    final XFile? image;
    if (op == 1) {
      image = await _picker.pickImage(source: ImageSource.camera);
    } else {
      image = await _picker.pickImage(source: ImageSource.gallery);
    }

    if (image == null) return;
    setState(() {
      imagen = File.fromUri(Uri(path: image?.path));
      _busy = true;
    });

    await analyzeTFLite();
    setState(() {
      imagen = File.fromUri(Uri(path: image?.path));

      _busy = false;
    });
  }

  void increase(String value) {
    counter++;
  }

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    Size size = mediaQuery.size;
    print(size.height.toString());
    return DefaultTabController(
      initialIndex: 0,
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 192, 204, 218),
          title: const Text(
            'Recicleytor',
          ),
          actions: [
            IconButton(
              padding: const EdgeInsets.only(right: 25),
              icon: const Icon(
                Icons.menu,
                size: 35,
              ),
              tooltip: 'Show Snackbar',
              onPressed: () {
                ScaffoldMessenger.of(context)
                    .showSnackBar(const SnackBar(content: Text('SOY EL MENU')));
              },
            ),
          ],
          bottom: const TabBar(
            tabs: <Widget>[
              Tab(
                text: 'Identificar',
              ),
              Tab(
                text: 'Informacion',
              ),
              Tab(
                text: 'Contenedores',
              ),
            ],
            labelColor: Colors.black,
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                height: size.height * .7,
                child: TabBarView(
                  children: <Widget>[
                    Center(
                        child: (imagen == null)
                            ? const Image(
                                image: AssetImage('assets/no-image.png'))
                            : Image.file(
                                File(imagen!.path.toString()),
                                fit: BoxFit.fill,
                              )),
                    Center(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Card(
                            color: Colors.white70,
                            clipBehavior: Clip.hardEdge,
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                debugPrint('Card tapped.');
                              },
                              child: SizedBox(
                                width: size.width * .5,
                                height: size.height * .1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('RESIDUO',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge),
                                    GeneraResiduo(
                                      counter: counter,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Card(
                            color: Colors.white70,
                            clipBehavior: Clip.hardEdge,
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                debugPrint('Card tapped.');
                              },
                              child: SizedBox(
                                width: size.width * .5,
                                height: size.height * .1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('TIPO',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge),
                                    GeneraTipo(counter: counter),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Card(
                            color: Colors.white70,
                            clipBehavior: Clip.hardEdge,
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                debugPrint('Card tapped.');
                              },
                              child: SizedBox(
                                width: size.width * .5,
                                height: size.height * .1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('CONTENEDOR',
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge),
                                    GeneraColorContenedor(counter: counter),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Card(
                            color: Colors.white70,
                            clipBehavior: Clip.hardEdge,
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                debugPrint('Card tapped.');
                              },
                              child: SizedBox(
                                width: size.width * .5,
                                height: size.height * .1,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'RECICLABLE',
                                      style:
                                          Theme.of(context).textTheme.titleLarge,
                                    ),
                                    GeneraReciclable(counter: counter)
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Card(
                            color: Colors.white70,
                            clipBehavior: Clip.hardEdge,
                            child: InkWell(
                              splashColor: Colors.blue.withAlpha(30),
                              onTap: () {
                                debugPrint('Card tapped.');
                              },
                              child: SizedBox(
                                width: size.width * .5,
                                height: size.height * .2,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Center(
                                      child: Text('INFORMACION',
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleLarge),
                                    ),
                                    GeneraInformacion(counter: counter),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    const Center(
                      child: _VistaContenedores(),
                    ),
                  ],
                ),
              ),
              _BarraDeIconosAcciones(
                elegirFoto: chooseImageGallery,
              )
            ],
          ),
        ),
        // backgroundColor: Colors.deepOrange,
      ),
    );
  }
}

class GeneraResiduo extends StatelessWidget {
  const GeneraResiduo({
    Key? key,
    required this.counter,
  }) : super(key: key);

  final int counter;

  @override
  Widget build(BuildContext context) {
    return Text((counter == 1)
        ? 'Inorganico'
        : (counter == 2)
            ? 'Inorganico'
            : (counter == 3)
                ? 'Inorganico'
                : (counter == 4)
                    ? 'Organico'
                    : 'sin identificar');
  }
}

class GeneraTipo extends StatelessWidget {
  const GeneraTipo({
    Key? key,
    required this.counter,
  }) : super(key: key);

  final int counter;

  @override
  Widget build(BuildContext context) {
    return Text((counter == 1)
        ? 'Papel y carton'
        : (counter == 2)
            ? 'Plasticos'
            : (counter == 3)
                ? 'Vidrio'
                : (counter == 4)
                    ? 'Restos de de origen animal o vegetal'
                    : 'sin identificar');
  }
}

class GeneraReciclable extends StatelessWidget {
  const GeneraReciclable({
    Key? key,
    required this.counter,
  }) : super(key: key);

  final int counter;

  @override
  Widget build(BuildContext context) {
    return Text((counter == 1)
        ? 'Si'
        : (counter == 2)
            ? 'Si'
            : (counter == 3)
                ? 'SI'
                : (counter == 4)
                    ? 'No'
                    : 'sin identificar');
  }
}

class GeneraColorContenedor extends StatelessWidget {
  const GeneraColorContenedor({
    Key? key,
    required this.counter,
  }) : super(key: key);

  final int counter;

  @override
  Widget build(BuildContext context) {
    return Text((counter == 1)
        ? 'Azul'
        : (counter == 2)
            ? 'amarillo'
            : (counter == 3)
                ? 'verde'
                : (counter == 4)
                    ? 'Anaranjado'
                    : 'sin identificar');
  }
}

class GeneraInformacion extends StatelessWidget {
  const GeneraInformacion({
    Key? key,
    required this.counter,
  }) : super(key: key);

  final int counter;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text((counter == 1)
            ? 'Mediante el reciclaje de una tonelada de papel se salvan entre 12 y 16 árboles medianos, se ahorran 50.000 litros de agua y más de 300 kg de petróleo.'
            : (counter == 2)
                ? 'Los envases de plástico se pueden reciclar para la fabricación de bolsas de plástico, mobiliario urbano, o bien para la obtención de nuevos envases de uso no alimentario (lejía, detergente, etc.)'
                : (counter == 3)
                    ? 'Según los ecologistas, el utilizar el contenedor verde para reciclaje vidrio supone una disminución de la contaminación atmosférica del 20%, una disminución del consumo de energía del 27% y de la contaminación de las aguas del 50%. Es basura reciclable al 100%!'
                    : (counter == 4)
                        ? 'Los residuos orgánicos, sirven para elaborar compost a través de su descomposición, un abono natural que aporta nutrientes a la tierra. La descomposición lenta de la materia orgánica es un proceso biológico en el que intervienen los hongos, las bacterias, lombrices y otros.'
                        : 'sin identificar'),
        Row(
          children: [
            const Icon(
              Icons.directions,
              color: Colors.blueAccent,
            ),
            const Text(
              'Av. Tlahuac 5662, Área Federal Panteón San Lorenzo Tezonco, Iztapalapa, 09790 CDMX',
            ),
          ],
        ),
        const Icon(Icons.currency_exchange_sharp),
        const Text(
          '.30 centavos o 1 punto',
          style: TextStyle(fontSize: 30),
        )
      ],
    );
  }
}

class _VistaContenedores extends StatelessWidget {
  const _VistaContenedores({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 20),
          child: Text('Selecciona el contenedor de tu interes.'),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                'assets/images/tacho-de-reciclaje.png',
                scale: 5,
              ),
              Image.asset(
                'assets/images/papelera-de-reciclaje.png',
                scale: 5,
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                'assets/images/contenedor-de-plastico.png',
                scale: 5,
              ),
              Image.asset(
                'assets/images/papelera-de-reciclaje_2.png',
                scale: 5,
                color: Colors.amber[900],
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 50),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/papelera-de-reciclaje_2.png',
                scale: 5,
              ),
            ],
          ),
        )
      ],
    );
  }
}

class _BarraDeIconosAcciones extends StatelessWidget {
  final Function elegirFoto;
  const _BarraDeIconosAcciones({
    Key? key,
    required this.elegirFoto,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                onPressed: () => elegirFoto(1),
                icon: const Icon(
                  Icons.camera_alt_outlined,
                  size: 30,
                ),
              ),
              const Text('Camara')
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              IconButton(
                onPressed: () => elegirFoto(2),
                icon: const Icon(
                  Icons.photo,
                  size: 30,
                ),
              ),
              const Text('Galeria')
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: const [
              IconButton(
                onPressed: null,
                icon: Icon(
                  Icons.save_rounded,
                  size: 30,
                ),
              ),
              Text('Guardar')
            ],
          ),
        ],
      ),
    );
  }
}
