// import 'dart:io';

// import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:tflite/tflite.dart';

// class HomeScreen extends StatefulWidget {
//   const HomeScreen({super.key});

//   @override
//   State<HomeScreen> createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//  int counter = 0;

//   File imagen = File("");
//   bool _busy = false;
//   String ruta = "";
//   late List _recognitions = [];

//   Future analyzeTFLite() async {
//     String? res = await Tflite.loadModel(
//         model: "assets/model.tflite",
//         labels: "assets/labels.txt",
//         numThreads: 2 // defaults to 1
//         );
//     print('Model Loaded: $res');
//     var recognitions = await Tflite.runModelOnImage(path: imagen.path);
//     setState(() {
//       _recognitions = recognitions!;
//     });
//     print('Recognition Result: $_recognitions');
//   }

//   Future chooseImageGallery(int op) async {
//     final ImagePicker _picker = ImagePicker();
//     final XFile? image;
//     if (op == 1) {
//       image = await _picker.pickImage(source: ImageSource.camera);
//     } else {
//       image = await _picker.pickImage(source: ImageSource.gallery);
//     }

//     if (image == null) return;
//     setState(() {
//       imagen = File.fromUri(Uri(path: image?.path));
//       _busy = true;
//     });

//     await analyzeTFLite();
//     setState(() {
//       imagen = File.fromUri(Uri(path: image?.path));

//       _busy = false;
//     });
//   }

//   void increase(String value) {
//     counter++;
//   }


//   @override
//   Widget build(BuildContext context) {
//     var mediaQuery = MediaQuery.of(context);
//     Size size = mediaQuery.size;
//     print(size.height.toString());
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: const Color.fromARGB(255, 192, 204, 218),
//         title: const Text(
//           'Recicleytor',
//         ),
//       ),
//       body: SingleChildScrollView(
//         child: Column(
//           children: [
//             SizedBox(
//               width: double.infinity,
//               height: size.height * .75,
//             ),
//             _BarraDeIconosAcciones(
//               elegirFoto: chose,
//             )
//           ],
//         ),
//       ),
//       // backgroundColor: Colors.deepOrange,
//     );
//   }
// }

// class _BarraDeIconosAcciones extends StatelessWidget {
//   final Function elegirFoto;
//   const _BarraDeIconosAcciones({
//     Key? key,
//     required this.elegirFoto,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       // color: Colors.amber,
//       child: Padding(
//         padding: const EdgeInsets.symmetric(horizontal: 30),
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: [
//             Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 IconButton(
//                   onPressed: () => elegirFoto(1),
//                   icon: const Icon(
//                     Icons.camera_alt_outlined,
//                     size: 30,
//                   ),
//                 ),
//                 Text('Camara')
//               ],
//             ),
//             Column(
//               mainAxisSize: MainAxisSize.min,
//               children: [
//                 IconButton(
//                   onPressed: () => elegirFoto(2),
//                   icon: const Icon(
//                     Icons.photo,
//                     size: 30,
//                   ),
//                 ),
//                 const Text('Galeria')
//               ],
//             ),
//             Column(
//               mainAxisSize: MainAxisSize.min,
//               children: const [
//                 IconButton(
//                   onPressed: null,
//                   icon: Icon(
//                     Icons.save_rounded,
//                     size: 30,
//                   ),
//                 ),
//                 Text('Guardar')
//               ],
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
