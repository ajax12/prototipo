import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registrate'),
        backgroundColor: const Color.fromARGB(255, 192, 204, 218),
        actions: [
          Image.asset(
            'assets/image_4.png',
            color: Colors.white,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 50,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  const TextField(
                    keyboardType: TextInputType.emailAddress,
                    autocorrect: false,
                    decoration: InputDecoration(
                      hintText: 'Username or email',
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  const TextField(
                    autocorrect: false,
                    decoration: InputDecoration(
                      hintText: 'password',
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Column(
                    children: [
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 5, vertical: 5),
                        color: const Color.fromARGB(255, 71, 82, 94),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            TextButton(
                                onPressed: () {
                                  Navigator.pushNamed(context, 'home');
                                },
                                child: const Text(
                                  'LOGIN',
                                  style: TextStyle(
                                      fontSize: 17, color: Colors.white),
                                ))
                          ],
                        ),
                      )
                    ],
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text('OR'),
                  ),
                  const Divider(
                    height: 25,
                    thickness: 1,
                    indent: 50,
                    endIndent: 50,
                    color: Color.fromARGB(255, 212, 209, 209),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
            Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 10),
                    color: const Color.fromARGB(255, 71, 82, 94),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/image_2.png',
                          color: Colors.white,
                        ),
                        const SizedBox(
                          width: 80,
                        ),
                        const Text(
                          'LOGIN WITH FACEBOOK',
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 10),
                    color: const Color.fromARGB(255, 132, 146, 166),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/image_1.png',
                          color: Colors.white,
                        ),
                        const SizedBox(
                          width: 80,
                        ),
                        const Text(
                          'LOGIN WITH TWITTER',
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            Column(
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 10),
                    color: const Color.fromARGB(255, 192, 204, 218),
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/image_3.png',
                          color: Colors.white,
                        ),
                        const SizedBox(
                          width: 80,
                        ),
                        const Text(
                          'LOGIN WITH GOOGLE',
                          style: TextStyle(fontSize: 17, color: Colors.white),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
